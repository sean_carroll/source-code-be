json.extract! issue, :id, :batch, :title, :assignee, :due_date, :weight, :confidential, :labels, :milestone, :issue_created_at, :url, :frontend, :backend, :priority, :severity, :infradev, :corrective_action, :observability, :performance, :security, :app_limits, :availability, :database, :deliverable, :created_at, :updated_at
json.url issue_url(issue, format: :json)
