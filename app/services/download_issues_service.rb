class DownloadIssuesService
  include HTTParty
  base_uri 'https://gitlab.com/api/v4'
  
  BASE_LABEL = "group::source code"
  LABELS = ["Application Limits","infradev","corrective action","Observability","performance","security","availability","database"]
  PROJECT_ID = 278964
  LABELLED_ISSUES = 'releases.csv'
  UNLABELLED_ISSUES = 'no_label_releases.csv'
  FIELDS = %w(iid title due_date weight confidential labels milestone created_at)
  META_FIELDS = %w(url frontend priority severity)
  LABEL_FIELDS = %w(infradev corrective_action observability performance security app_limits availability database deliverable)
  
  attr_reader :processed_ids

  def initialize
    @processed_ids = []
  end
  
  def call
    labelled_issues
    unlabelled_issues
  end
  
end



# releases.rb
# Download Create:Source Code issues related to SaaS Reliability

require 'date'
require "active_support"
require "active_support/all"
require 'csv'
require 'byebug'
require 'httparty'

class Releases
  include HTTParty
  base_uri 'https://gitlab.com/api/v4'
  
  BASE_LABEL = "group::source code"
  LABELS = ["Application Limits","infradev","corrective action","Observability","performance","security","availability","database"]
  PROJECT_ID = 278964
  LABELLED_ISSUES = 'releases.csv'
  UNLABELLED_ISSUES = 'no_label_releases.csv'
  FIELDS = %w(iid title due_date weight confidential labels milestone created_at)
  META_FIELDS = %w(url frontend priority severity)
  LABEL_FIELDS = %w(infradev corrective_action observability performance security app_limits availability database deliverable)

  attr_reader :processed_ids

  def initialize
    @processed_ids = []
  end
  
  def call
    labelled_issues
    unlabelled_issues
  end

  private
  
  def http_headers
    { 'PRIVATE-TOKEN' => private_token }
  end
  
  def private_token
    @private_token ||= File.read('api-key').chomp
  end

  def fetch_data(labels:, page: 1)
    query = "labels=#{labels.join(',')}&scope=all&state=opened&per_page=99&page=#{page}"
    url = "/projects/#{PROJECT_ID}/issues"
    puts url + query
    
    self.class.get(url, headers: http_headers, query: query, format: :json)
  end
  
  def private_token
    @private_token ||= File.read('api-key').chomp
  end
  
  def labelled_issues
    #csv = CSV.open(LABELLED_ISSUES, "w")
    #csv << ['username'] + FIELDS + META_FIELDS + LABEL_FIELDS
    csv = []
    
    LABELS.each do |label|
      puts "processing label: #{label}"
      get_issues(csv, label)
    end
    
    #csv.close
    
    puts "#{processed_ids.count} labelled issues \n#{processed_ids}"
  end
  
  def unlabelled_issues
    #csv = CSV.open(UNLABELLED_ISSUES, "w")
    #csv << FIELDS + META_FIELDS
    
    get_unlabelled_issues(csv)
    #csv.close
    
    puts "#{processed_ids.count} total issues \n#{processed_ids}"
  end

  def get_issues(csv, label, page: 1)
    puts "processing page: #{page}"
    response = fetch_data(labels: [label, BASE_LABEL], page: page)
    page_count = response.headers["x-total-pages"].to_i
    puts "page_count: #{page_count}"
    puts "#{response.headers["x-total"]} issues for label #{label}"

    response.each do |row|
      next if processed_ids.include?(row["id"])
      
      byebug
      processed_ids << row["id"]
      formatted = row[slice(*FIELDS).merge("milestone" => row["milestone"] && row["milestone"]["title"]).merge(meta_columns(row)).merge(label_columns(row["labels"]))

      csv << formatted.values
    end
    
    if page_count > page
      get_issues(csv, label, page: page + 1)
    end
  end
  
  def get_unlabelled_issues(csv, page: 1)
    puts "processing page: #{page}"
    response = fetch_data(labels: [BASE_LABEL], page: page)
    page_count = response.headers["x-total-pages"].to_i
    puts "page_count: #{page_count}"
    puts "#{response.headers["x-total"]} total issues"

    response.each do |row|
      next if processed_ids.include?(row["id"])
      
      processed_ids << row["id"]
      formatted = row.slice(*FIELDS).merge("milestone" => row["milestone"] && row["milestone"]["title"]).merge(meta_columns(row))

      csv << formatted.values
    end
    
    if page_count > page
      get_unlabelled_issues(csv, page: page + 1)
    end
  end
  
  def meta_columns(row)
    labels = row["labels"]
    priorities = ["priority::1", "priority::2", "priority::3", "priority::4"]
    severities = ["severity::1", "severity::2", "severity::3", "severity::4"]
    {
      url: "https://gitlab.com/gitlab-org/gitlab/-/issues/#{row["iid"]}",
      frontend: labels.include?("frontend"),
      priority: labels.detect {|elem| priorities.include?(elem)},
      severity: labels.detect {|elem| severities.include?(elem)}
    }
  end
  
  def label_columns(labels)
    {
      infradev: labels.include?("infradev") ? 'X' : nil,
      corrective_action: labels.include?("corrective action") ? 'X' : nil,
      observability: labels.include?("Observability") ? 'X' : nil,
      performance: labels.include?("performance") ? 'X' : nil,
      security: labels.include?("security") ? 'X' : nil,
      app_limits: labels.include?("Application Limits") ? 'X' : nil,
      availability: labels.include?("availability") ? 'X' : nil,
      database: labels.include?("database") ? 'X' : nil,
      deliverable: labels.include?("Deliverable") ? 'X' : nil,
    }
  end
end
