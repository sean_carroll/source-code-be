class CreateIssues < ActiveRecord::Migration[6.1]
  def change
    create_table :issues do |t|
      t.integer :batch
      t.string :title
      t.string :assignee
      t.date :due_date
      t.integer :weight
      t.boolean :confidential, default: false
      t.text :labels, array: true, default: []
      t.string :milestone
      t.date :issue_created_at
      t.string :url
      t.string :priority
      t.string :severity
      t.boolean :frontend, default: false
      t.boolean :backend, default: false
      t.boolean :infradev, default: false
      t.boolean :corrective_action, default: false
      t.boolean :observability, default: false
      t.boolean :performance, default: false
      t.boolean :security, default: false
      t.boolean :app_limits, default: false
      t.boolean :availability, default: false
      t.boolean :database, default: false
      t.boolean :engineering_allocation, default: false
      t.boolean :deliverable, default: false

      t.timestamps
    end
  end
end
