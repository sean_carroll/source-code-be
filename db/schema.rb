# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_05_221151) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "issues", force: :cascade do |t|
    t.integer "batch"
    t.string "title"
    t.string "assignee"
    t.date "due_date"
    t.integer "weight"
    t.boolean "confidential", default: false
    t.text "labels", default: [], array: true
    t.string "milestone"
    t.date "issue_created_at"
    t.string "url"
    t.string "priority"
    t.string "severity"
    t.boolean "frontend", default: false
    t.boolean "backend", default: false
    t.boolean "infradev", default: false
    t.boolean "corrective_action", default: false
    t.boolean "observability", default: false
    t.boolean "performance", default: false
    t.boolean "security", default: false
    t.boolean "app_limits", default: false
    t.boolean "availability", default: false
    t.boolean "database", default: false
    t.boolean "engineering_allocation", default: false
    t.boolean "deliverable", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
